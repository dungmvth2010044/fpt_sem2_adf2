package collection;
import java.util.LinkedList;

public class ListInterfaceWithLinkedList {
    public static void main(String[] args) {
        //Linh hoat hon Array, doi cho danh sach cac phan tu, khong bi gioi han phan tu
        //Danh sach lien ket dong ->
        LinkedList<String> myLinkedList = new LinkedList<String>();
        //can dung khi gap tinh huong can su bien doi sap sep doi cho
        myLinkedList.add("APPLE");
        myLinkedList.add("SAMSUNG");
        myLinkedList.add("NOKIA");
        System.out.println("List1: "+myLinkedList);

        myLinkedList.addFirst( "XIAOMI");
        myLinkedList.addLast("LG");
        myLinkedList.add(2,"BPHONE");
        System.out.println("List2: " +myLinkedList);

        myLinkedList.remove(0);
        //myLinkedList.removeFirst();
        myLinkedList.removeLast();
        Object listobject = myLinkedList.get(2);//dang tro vao vung nho co index =2
        myLinkedList.set(2,(String)listobject +"VPHONE");
        System.out.println("List3: "+myLinkedList);




    }
}
