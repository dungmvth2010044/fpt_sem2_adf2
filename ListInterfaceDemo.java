//su dung phu hop voi 1 tac vu
//nhieu tac vu la nhieu luong vd: dat do an, do uong, trang mieng thi moi loai no se la 1 luong
package collection;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
//import java.ultil.*;   //lay ra het ca ko phai khai bao tung cai 1
public class ListInterfaceDemo {
    public static void main(String[] args) {
        //ArrayList khai bao co 3 cach:
        /*
        ArrayList arrayList = new ArrayList();
        List list = new ArrayList();
        List<String> list1 = new ArrayList<String>();*/

        List<String> myList = new ArrayList<String>();
        myList.add("APPlE");
        myList.add("LG");
        myList.add("SAMSUNG");
        myList.add("NOKIA");
        myList.add("ASUS");



        ListIterator<String> listIterator =myList.listIterator();
       /* String string1 = listIterator.next();//start item 1 co index = 0
        System.out.println(string1);
        String string2 = listIterator.next();//start item 2 co index = 1
        System.out.println(string2);

        String string3 = listIterator.next();
        System.out.println(string3);

        if(listIterator.hasPrevious()){
            String string = listIterator.previous();
            System.out.println(string);
        } */
        while (listIterator.hasNext()) {
            String stringValue = listIterator.next();
            System.out.println(stringValue);
        }
    }
}
