package collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ListInterfaceWithHasSetDemo {
    public void main(String[] args){
        //khoi tao ban dau 1000 phan tu se chua
        //va tang len kha nang chua la 50%
        //Khong cho phep gia tri trubg lap, chap nhan kieu unique
        Set<String> mySet = new HashSet<String>(1000);//new toan tu cap phat vung nho
        mySet.add("APPLE");
        mySet.add("LG");
        mySet.add("APPLE");//khi in ra se ghi de len cai cu
        mySet.add("ASUS");
        mySet.add("SAMSUNG");

        Iterator<String> iterator = mySet.iterator();
        while ( (iterator.hasNext())){
            System.out.println(iterator.next());
        }

    }
}
